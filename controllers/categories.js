
const CategoriesModel = require('../models/categories')

// Get all categories
module.exports = getAll = (req, res, next) => {
  Promise.resolve(CategoriesModel.find({}))
    .then(categories => {
      res.send(categories)
    })
}
