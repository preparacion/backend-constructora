/**
 * config/index.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
'use strict'

const dotenv = require('dotenv');
const path = require('path');
const env = process.env.NODE_ENV || 'development';

const envPath = path.resolve(__dirname, `../.envs/${env}`);
dotenv.config({ path: envPath })

const configs = {
  base: {
    env,
    port: process.env.APP_PORT || 3000
  },
  development: {
    port: process.env.APP_PORT || 3000
  },
  production: {
    port: process.env.APP_PORT || 3000
  }
}

const config = Object.assign(configs.base, configs[env])
module.exports = config
