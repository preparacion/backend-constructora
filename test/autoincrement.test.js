const chai = require('chai');
const app = require('../app');
const chaiHttp = require('chai-http');

const { expect } = chai;
chai.use(chaiHttp);


describe('Function to increment estimacion count', () => {
  it('Should retun a integer', (done) => {
    chai
    .request(app)
    .get('/autoincrement/5db61d48cda8c72e1bb4d740/5dcf743f4aaa2e747e06a1ab')
    .end((req, res) => {
        expect(res.body).to.be.a('number');;
        done();
    })
  });
  it('Should retun correct number', () => {
    chai
    .request(app)
    .get('/autoincrement/5db61d48cda8c72e1bb4d740/5dcf743f4aaa2e747e06a1ab')
    .end((req, res) => {
        expect(res.body).to.equal(1);
        done();
    })
  });
});
