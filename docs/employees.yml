/employees:
  get:
    summary: GET ALL
    description: Get all the employees
    produces:
      - application/json
    tags:
      - Employees
    responses:
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            reports:
              type: array
              description: List of reports
              items:
                type: object
                properties:
                  _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                  name:
                    type: String
                    description: employee's name
                    example: Jorge
                  secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                  aPaterno:
                    type: String
                    description: employee's last name
                    example: Peréz
                  aMaterno:
                    type: String
                    description: employee's second last name
                    example: Ruiz
                  noSocial:
                    type: String
                    description: employee's social security number
                    example: 2323112123
                  curp:
                    type: String
                    description: employee's CURP
                    example: TRES2309438934
                  status:
                    type: Boolean
                    description: employee's status of social security
                    example: true
                  category:
                    type: Object ID
                    description: employee's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                  contratista:
                    type: Object ID
                    description: contratista's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                  obra:
                    type: Object ID
                    description: obra's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                  createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
  post:
    summary: CREATE
    description: Create a new employee
    produces:
      - application/json
    tags:
      - Employees
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: employee object schema
            properties:
              name:
                    type: String
                    description: employee's name
                    example: Ayudante nivel 2
              secondName:
                    type: String
                    description: employee's second name
                    example: Luis
              aPaterno:
                    type: String
                    description: employee's last name
                    example: Peréz
              aMaterno:
                    type: String
                    description: employee's second last name
                    example: Ruiz
              noSocial:
                    type: String
                    description: employee's social security number
                    example: 2323112123
              curp:
                    type: String
                    description: employee's CURP
                    example: TRES2309438934
              status:
                    type: Boolean
                    description: employee's status of social security
                    example: true
              category:
                    type: Object ID
                    description: employee's category object ID
                    example: 5c02cf14eefac11c7a6d0006
              contratista:
                    type: Object ID
                    description: contratista's category object ID
                    example: 5c02cf14eefac11c7a6d0006
              obra:
                    type: Object ID
                    description: obra's category object ID
                    example: 5c02cf14eefac11c7a6d0006
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Juan
                secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                aPaterno:
                    type: String
                    description: employee's last name
                    example: Peréz
                aMaterno:
                    type: String
                    description: employee's second last name
                    example: Ruiz
                noSocial:
                    type: String
                    description: employee's social security number
                    example: 2323112123
                curp:
                    type: String
                    description: employee's CURP
                    example: TRES2309438934
                status:
                    type: Boolean
                    description: employee's status of social security
                    example: true
                category:
                    type: Object ID
                    description: employee's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                contratista:
                    type: Object ID
                    description: contratista's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                obra:
                    type: Object ID
                    description: obra's category object ID
                    example: 5c02cf14eefac11c7a6d0006
                files:
                  type: array
                  items:
                      type: object
                      properties:
                        _id:
                          type: objectId
                          description: file's object id
                          example: 5c02cf14eefac11c7a6d0006
                        name:
                          type: String
                          description: file's name
                          example: lista empleados 1
                        date:
                          type: Date
                          description: date of file was saved
                          example: 2019-09-24T23:28:42.586Z
                        url:
                          type: String
                          description: URL of image
                          example: https://storage.googleapis.com/consttest/Portada-ulala-senor-frances.jpg
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
/employees/id:
  get:
    summary: GET BY ID
    description: Get just one object
    produces:
      - application/json
    tags:
      - Employees
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Jorge
                secondName:
                    type: String
                    description: employee's second name
                    example: Juan
                aPaterno:
                    type: String
                    description: employee's last name
                    example: Peréz
                aMaterno:
                    type: String
                    description: employee's second last name
                    example: Ruiz
                noSocial:
                    type: String
                    description: employee's social security number
                    example: 2323112123
                curp:
                    type: String
                    description: employee's CURP
                    example: TRES2309438934
                status:
                    type: Boolean
                    description: employee's status of social security
                    example: true
                category:
                    type: object
                    properties:
                      _id:
                          type: objectId
                          description: employee's object id
                          example: 5c02cf14eefac11c7a6d0006
                      name:
                        type: String
                        description: Employee's name
                        example: Ayudante nivel 2
                      monto:
                        type: Number
                        description: Employee's amount of salary
                        example: 555
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
                comments:
                  type: object
                  properties:
                      _id:
                        type: objectId
                        description: comment's object id
                        example: 5c02cf14eefac11c7a6d0006
                      comment:
                        type: String
                        description: Employee's name
                        example: El empleado subió de categoría
                      date:
                        type: Date
                        description: Employee's amount of salary
                        example: 2019-09-24T23:28:42.586Z
                contratista:
                  type: object
                  properties:
                      _id:
                        type: objectId
                        description: comment's object id
                        example: 5c02cf14eefac11c7a6d0006
                      nombre:
                        type: String
                        description: contratista's name
                        example: El empleado subió de categoría
                obra:
                  type: object
                  properties:
                    _id:
                      type: objectId
                      description: obra's object ID
                      example: 5c02cf14eefac11c7a6d0006
                    name:
                      type: String
                      description: Obra's name
                      example: Obra 123
  put:
    summary: UPDATE
    description: Update a employee, you can use all fields or few of them
    produces:
      - application/json
    tags:
      - Employees
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: employee object schema
            properties:
              name:
                    type: String
                    description: employee's name
                    example: Juan
              secondName:
                    type: String
                    description: employee's second name
                    example: Luis
              aPaterno:
                    type: String
                    description: employee's last name
                    example: Peréz
              aMaterno:
                    type: String
                    description: employee's second last name
                    example: Ruiz
              noSocial:
                    type: String
                    description: employee's social security number
                    example: 2323112123
              curp:
                    type: String
                    description: employee's CURP
                    example: TRES2309438934
              status:
                    type: Boolean
                    description: employee's status of social security
                    example: true
              category:
                    type: Object ID
                    description: employee's category object ID
                    example: 5c02cf14eefac11c7a6d0006
              
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Juan
                secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                aPaterno:
                      type: String
                      description: employee's last name
                      example: Peréz
                aMaterno:
                      type: String
                      description: employee's second last name
                      example: Ruiz
                noSocial:
                      type: String
                      description: employee's social security number
                      example: 2323112123
                curp:
                      type: String
                      description: employee's CURP
                      example: TRES2309438934
                status:
                      type: Boolean
                      description: employee's status of social security
                      example: true
                category:
                      type: Object ID
                      description: employee's category object ID
                      example: 5c02cf14eefac11c7a6d0006
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
  delete:
    summary: DELETE
    description: Update a employee, you can use all fields or any of them
    produces:
      - application/json
    tags:
      - Employees
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Juan
                secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                aPaterno:
                      type: String
                      description: employee's last name
                      example: Peréz
                aMaterno:
                      type: String
                      description: employee's second last name
                      example: Ruiz
                noSocial:
                      type: String
                      description: employee's social security number
                      example: 2323112123
                curp:
                      type: String
                      description: employee's CURP
                      example: TRES2309438934
                status:
                      type: Boolean
                      description: employee's status of social security
                      example: true
                category:
                      type: Object ID
                      description: employee's category object ID
                      example: 5c02cf14eefac11c7a6d0006
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
                    
/employees/addComment/id:
  put:
    summary: ADD COMMENT
    description: Add comment to employee
    produces:
      - application/json
    tags:
      - Employees
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: employee object schema
            properties:
              comment:
                    type: String
                    description: employee's comment
                    example: El empleado se cambió de categoría a una mayor
              
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Juan
                secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                aPaterno:
                      type: String
                      description: employee's last name
                      example: Peréz
                aMaterno:
                      type: String
                      description: employee's second last name
                      example: Ruiz
                noSocial:
                      type: String
                      description: employee's social security number
                      example: 2323112123
                curp:
                      type: String
                      description: employee's CURP
                      example: TRES2309438934
                status:
                      type: Boolean
                      description: employee's status of social security
                      example: true
                category:
                      type: Object ID
                      description: employee's category object ID
                      example: 5c02cf14eefac11c7a6d0006
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
                updatedAt:
                    type: Date
                    description: employee's date of last update
                    example: 2019-09-24T23:28:42.586Z
                comments:
                  type: object
                  properties:
                      _id:
                        type: objectId
                        description: comment's object id
                        example: 5c02cf14eefac11c7a6d0006
                      comment:
                        type: String
                        description: Employee's name
                        example: El empleado subió de categoría
                      date:
                        type: Date
                        description: Employee's amount of salary
                        example: 2019-09-24T23:28:42.586Z
/employees/status/id:
  put:
    summary: EDIT STATUS
    description: Edit status from employee
    produces:
      - application/json
    tags:
      - Employees
    responses: 
      200:
        description: Ok
        schema:
          type: object
          properties:
            report:
              type: object
              properties:
                _id:
                    type: objectId
                    description: employee's object id
                    example: 5c02cf14eefac11c7a6d0006
                name:
                    type: String
                    description: employee's name
                    example: Juan
                secondName:
                    type: String
                    description: employee's second name
                    example: Luis
                aPaterno:
                      type: String
                      description: employee's last name
                      example: Peréz
                aMaterno:
                      type: String
                      description: employee's second last name
                      example: Ruiz
                noSocial:
                      type: String
                      description: employee's social security number
                      example: 2323112123
                curp:
                      type: String
                      description: employee's CURP
                      example: TRES2309438934
                status:
                      type: Boolean
                      description: employee's status of social security
                      example: true
                category:
                      type: Object ID
                      description: employee's category object ID
                      example: 5c02cf14eefac11c7a6d0006
                createdAt:
                    type: Date
                    description: employee's date of creation
                    example: 2019-09-24T23:28:42.586Z
                updatedAt:
                    type: Date
                    description: employee's date of last update
                    example: 2019-09-24T23:28:42.586Z
                comments:
                  type: object
                  properties:
                      _id:
                        type: objectId
                        description: comment's object id
                        example: 5c02cf14eefac11c7a6d0006
                      comment:
                        type: String
                        description: Employee's name
                        example: El empleado subió de categoría
                      date:
                        type: Date
                        description: Employee's amount of salary
                        example: 2019-09-24T23:28:42.586Z