const express = require('express')
const router = express.Router()

// PUBLIC

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send('API bien padre creada por Bluedot');
});
/** docsApi */
const docsApi = require('./api/docs');
router.use('/docs', docsApi);

// PROTECTED
/** categoriesApi */
const categoriesApi = require('./api/categories');
router.use('/api/categories', categoriesApi);
/** contractorsApi */
const conceptsApi = require('./api/concepts');
router.use('/api/concepts', conceptsApi);
// employee routes
const employeesApi = require('./api/employees');
router.use('/api/employees', employeesApi);
// obras routes
const obrasAPI = require('./api/obra');
router.use('/api/obras', obrasAPI);
// contratistas routes
const contratistasAPI = require('./api/contratistas');
router.use('/api/contratistas', contratistasAPI);
// catalog routes
const catalogAPI = require('./api/catalog');
router.use('/api/catalog', catalogAPI);
// estimaciones routes
const estimacionesAPI = require('./api/estimaciones');
router.use('/api/estimaciones', estimacionesAPI);
// excel routes
const excelAPI = require('./api/excel');
router.use('/api/', excelAPI);

const fileAPI = require('./api/file');
router.use('/api/file', fileAPI);

const quizAPI = require('./api/quiz');
router.use('/api/quiz', quizAPI);

const autoincrement = require('../helpers/autoincrement');
router.use('/api/autoincrement', autoincrement);

module.exports = router;
