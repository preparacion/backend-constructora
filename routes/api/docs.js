/**
 * routes/api/docs.js
 *
 * @description :: Define doc routes
 * @docs        :: TODO
 */
const fs = require("fs");
const path = require("path");
const express = require("express");
const swaggerJSDoc = require("swagger-jsdoc");
const router = express.Router();

// -- setup up swagger-jsdoc --
const swaggerDefinition = {
  servers: [
    {
      url: "https://localhost:3000/api/",
      description: "Dev server"
    },
    {
      url: "https://localhost:3000/api",
      description: "Production server"
    }
  ],
  info: {
    title: "Constructora",
    version: "1.1.1",
    description: "Backend api routes",
    contact: {
      name: "API support"
    }
  },
  host: "localhost:3000",
  basePath: "/api"
};

const options = {
  apis: [
    path.resolve(__dirname, "../../docs/categories.yml"),
    path.resolve(__dirname, "../../docs/concepts.yml"),
    path.resolve(__dirname, "../../docs/employees.yml"),
    path.resolve(__dirname, "../../docs/obras.yml"),
    path.resolve(__dirname, "../../docs/contratistas.yml"),
    path.resolve(__dirname, "../../docs/catalog.yml"),
    // path.resolve(__dirname, "../../docs/estimaciones.yml"),
    path.resolve(__dirname, "../../docs/files.yml")
  ],
  swaggerDefinition
};

const swaggerSpec = swaggerJSDoc(options);

router
  .get("/", function(req, res, next) {
    res.render("docs", { title: "HEG" });
  })
  .get("/download/swagger.json", (req, res, next) => {
    res.set("Content-Type", "application/json");
    res.send(swaggerSpec);
  });

module.exports = router;
