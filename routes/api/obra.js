/* eslint-disable no-param-reassign */
/**
 * routes/api/categories.js
 *
 * @description :: Describes the categories routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
// const debug = require('debug')('backend-constructora:db');
const mongoose = require('mongoose');
const ObrasModel = require('../../models/obra');
const ContratistasModel = require('../../models/contratistas');
const listaEmpleados = require('../../models/listaEmpleados');

router
  /* GET all the categories */
  .get('/', async (req, res) => {
    try {
      const allObras = await ObrasModel.find({ deleted: false }).populate('contratistas', '_id nombre apellidoP apellidoM').exec();
      const lists = await listaEmpleados.find().exec();
      for (obra of allObras) {
        for (contratista of obra.contratistas) {
          for (lista of lists) {
            if(lista.hasOwnProperty(obra)){
              console.log(`Lista.obra ${lista._id} Obra ${obra._id} Contratista ${contratista._id}`);
              if (lista.obra.equals(obra._id) && lista.contratista.equals(contratista._id)) {
                contratista.empleados = 'lista.lista.length';
              }
            }
          }
        }
      }
      res.status(200).send(allObras);
    } catch (err) {
      console.log(`Èrror en obras ${err}`);
      res.status(500).send(err);
    }
  })

  /* Create a new obra */
  .post('/', (req, res) => {
    const obra = new ObrasModel(req.body);
    Promise.resolve(obra.save())
      .then(() => {
        res.status(201).send(obra);
      });
  });

router
  /* Get obra */
  .get('/:id', (req, res) => {
    Promise.resolve(ObrasModel.find({ _id: req.params.id, deleted: false }).populate('contratistas', '_id nombre apellidoP apellidoM').exec())
      .then((obra) => {
        if (obra.length) {
          res.status(200).send(obra[0]);
        } else {
          res.status(200).send('No se ha encontrado la categoría');
        }
      }).catch((error) => res.status(400).send(`Ha ocurrido un problema al buscar la categoría ${error}`));
  })
  /* Update obra */
  .put('/:id', (req, res) => {
    ObrasModel.findById(mongoose.Types.ObjectId(req.params.id), (error, obra) => {
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
      } else if (obra) {
        obra.name = req.body.name || obra.name;
        obra.address = req.body.address || obra.address;
        obra.save((err, obraSaved) => {
          res.status(201).send(obraSaved);
        });
      } else {
        res.status(404).send('No se ha encontrado la categoría');
      }
    });
  })
  /* Delete obra */
  .delete('/:id', (req, res) => {
    ObrasModel.findById(mongoose.Types.ObjectId(req.params.id), (error, obra) => {
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
      } else if (obra) {
        obra.deleted = true;
        obra.save((err, obraSaved) => {
          res.status(201).send(obraSaved);
        });
      } else {
        res.status(200).send('No se ha encontrado la obra');
      }
    });
  })
  .put('/addContratistaToObra/:id', async (req, res) => {
    try {
      const obra = await ObrasModel.findById(mongoose.Types.ObjectId(req.params.id));
      const contratista = await ContratistasModel.findById(mongoose.Types.ObjectId(req.body.contratista));
      if (obra == null || contratista == null) {
        res.status(404).send('Elemento no encontrado');
      } else {
        contratista.obras.push(mongoose.Types.ObjectId(req.params.id));
        obra.contratistas.push(mongoose.Types.ObjectId(req.body.contratista));
        obra.save((err, obraSaved) => {
          if (err) {
            res.status(500).send('Error al guardar la obra');
          } else {
            contratista.save((error, contratistaSaved) => {
              if (error) {
                res.status(500).send('Error al guardar el contratista');
              } else {
                res.status(201).send(obraSaved);
              }
            });
          }
        });
      }
    } catch (error) {
      res.status(500).send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
    }
  });

module.exports = router;
