/**
 * routes/api/categories.js
 *
 * @description :: Describes the categories routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
// const debug = require('debug')('backend-constructora:db');
const ConceptsModel = require('../../models/concepts');

router
  /* GET all the concepts */
  .get('/', (req, res) => {
    Promise.resolve(ConceptsModel.find({ deleted: false }))
      .then((concepts) => {
        res.send(concepts);
      });
  })
  /* Create a new concept */
  .post('/', (req, res) => {
    const concept = new ConceptsModel(req.body);
    concept.clave = `${req.body.familia}-${req.body.clave}`;
    Promise.resolve(concept.save())
      .then(() => {
        res.status(201).send(concept);
      });
  });

router
  /* Get concept */
  .get('/:id', (req, res) => {
    Promise.resolve(ConceptsModel.find({ _id: req.params.id }))
      .then((concept) => {
        if (concept.length) {
          res.status(200).send(concept);
        } else {
          res.status(200).send('No se ha encontrado el concepto');
        }
      }).catch((error) => res.status(400).send(`Ha ocurrido un problema al buscar el concepto ${error}`));
  })
  /* Update concept */
  .put('/:id', (req, res) => {
    ConceptsModel.findById(req.params.id, (error, concept) => {
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar el concepto ${error}`);
      } else if (concept) {
        concept.pu = req.body.pu || concept.pu;
        concept.clave = `${req.body.familia}-${req.body.clave}` || concept.clave;
        concept.concepto = req.body.concepto || concept.concepto;
        concept.unidad = req.body.unidad || concept.unidad;
        concept.save((err, conceptSaved) => {
          res.status(201).send(conceptSaved);
        });
      } else {
        res.status(404).send('No se ha encontrado el concepto');
      }
    });
  })
  /* Delete concept */
  .delete('/:id', (req, res) => {
    ConceptsModel.findById(req.params.id, (error, concept) => {  
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar el concepto ${error}`);
      } else if (concept) {
        concept.deleted = true;
        concept.save((err, caterySaved) =>{
          res.status(201).send(caterySaved);
        });
      } else {
        res.status(200).send('No se ha encontrado el concepto');
      }
    });
  });

module.exports = router;
