/* eslint-disable no-param-reassign */
/**
 * routes/api/contratistas.js
 *
 * @description :: Describes the contratistas routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
// const debug = require('debug')('backend-constructora:db');
const mongoose = require('mongoose');
let xl = require('excel4node');
const EmployeesModel = require("../../models/employees");

const ContratistasModel = require('../../models/contratistas');
const listaEmpleados = require('../../models/listaEmpleados');

router
  /* GET all the contratistas */
  .get('/', async (req, res) => {
    try {
      const allContratistas = await ContratistasModel.find({ deleted: false });
      res.status(200).send(allContratistas);
    } catch (err) {
      res.status(500).send(err);
    }
  })
  
  .get('/contratistasConObras/', async (req, res) => {
    try {
      const contratistas = await ContratistasModel.find({ deleted: false })
        .populate('obras', 'name')
        .exec();
      if (contratistas) {
        res.status(200).send(contratistas);
      } else {
        res.status(404).send('No se encontró el empleado');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el empleado ${err}`);
    }
  })
  /* Create a new contratista */
  .post('/', (req, res) => {
    const employee = new EmployeesModel(req.body);
    employee.category = mongoose.Types.ObjectId('5dfc04b9cbb8430c2c4c42c3');
    const contratista = new ContratistasModel(req.body);
    Promise.resolve(employee.save()).then(employee => {
      contratista.employee = employee._id;
      Promise.resolve(contratista.save()).then((contratista) => {
          Promise.resolve(
              listaEmpleados.findOne({
                contratista: contratista._id,
                obra: req.body.obra
              })
            ).then(lista => {
              if (lista) {
                lista.lista.push(employee._id);
                Promise.resolve(lista.save()).then(lista => {
                  res.status(201).send(lista);
                });
              } else {
                const listaEmployees = new listaEmpleados({
                  contratista: contratista._id,
                  obra: req.body.obra,
                  lista: [employee._id]
                });
                Promise.resolve(listaEmployees.save()).then(lista => {
                  res.status(201).send(lista);
                });
              }
            });
          });
    });
  });

router
.get('/lista/:idObra/:idContratista', (req, res) => {
  Promise.resolve(
    listaEmpleados
      .findOne({
        obra: mongoose.Types.ObjectId(req.params.idObra),
        contratista:mongoose.Types.ObjectId(req.params.idContratista) ,
      })
      .populate({
        path: 'lista',
        populate: {
          path: 'category',
        },
      })
      .exec()
      .then( async (lista) => {
        res.status(200).send(lista.lista);
        // if (lista) {
        //   console.log('entraaaaaamos');
        //   const contratista = await EmployeesModel.findOne({
        //     _id: req.params.idContratista,
        //     deleted: false
        //   })
        //   .populate('employee')
        //   .exec();
        //   console.log(`contratista ${contratista}`);
        //   lista.lista.push(contratista.employee);
        //   console.log(`lista lista ${lista.lista}`);
        //   res.status(200).send(lista.lista);
        // } else {
        //   console.log('okok');
        //   res.status(404).send([]);
        // }
      })
      .catch((error) => res.status(400).send(error)),
  );
})
  /* Get contratista */
  .get('/:id', (req, res) => {
    Promise.resolve(
      ContratistasModel.find({ _id: req.params.id, deleted: false }),
    )
      .then((contratista) => {
        if (contratista.length) {
          res.status(200).send(contratista[0]);
        } else {
          res.status(200).send('No se ha encontrado la categoría');
        }
      })
      .catch((error) => res
          .status(400)
          .send(`Ha ocurrido un problema al buscar el contratista ${error}`),);
  })
  /* Update contratista */
  .put('/:id', async (req, res) => {
    try{
      let contratistaData = await ContratistasModel.findById(mongoose.Types.ObjectId(req.params.id));
      if (contratistaData) {
          contratistaData.name = req.body.name || contratistaData.name;
          contratistaData.secondName = req.body.secondName || contratistaData.secondName;
          contratistaData.aPaterno = req.body.aPaterno || contratistaData.aPaterno;
          contratistaData.aMaterno = req.body.aMaterno || contratistaData.aMaterno;
          contratistaData.noSocial = req.body.noSocial || contratistaData.noSocial;
          contratistaData.type = req.body.type || contratistaData.type;
          contratistaData.rfc = req.body.rfc || contratistaData.rfc;
          EmployeesModel.findById(contratistaData.employee, (error, employee) => {
            if (error) {
              res
                .status(500)
                .send(`Ha ocurrido un problema al editar el contratista ${error}`);
            } else if (employee) {
              employee.name = contratistaData.name;
              employee.secondName = contratistaData.secondName || employee.secondName;
              employee.aMaterno = contratistaData.aMaterno || employee.aMaterno;
              employee.aPaterno = contratistaData.aPaterno || employee.aPaterno;
              employee.noSocial = contratistaData.noSocial || employee.noSocial;
              contratistaData.save((err, contratistaSaved) => {
                employee.save((err, employeeSaved) => {
                  res.status(201).send(contratistaSaved);
                });              
              });
            } else {
              res.status(404).send("No se ha encontrado la empleado");
            }
          });
          
      } else {
          res.status(404).send('No se ha encontrado la categoría');
      }
    }catch(error){
      res
        .status(500)
        .send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
    }
  })
  /* Delete contratista */
  .delete('/:id', (req, res) => {
    ContratistasModel.findById(
      mongoose.Types.ObjectId(req.params.id),
      (error, contratista) => {
        if (error) {
          res
            .status(500)
            .send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
        } else if (contratista) {
          contratista.deleted = true;
          contratista.save((err, contratistasaved) => {
            res.status(201).send(contratistasaved);
          });
        } else {
          res.status(200).send('No se ha encontrado la contratista');
        }
      },
    );
  });
  

module.exports = router;
