const express = require('express');

const router = express.Router();
// const debug = require('debug')('backend-constructora:db');
const mongoose = require('mongoose');
const xl = require('excel4node');
const listaEmpleados = require('../../models/listaEmpleados');
const contratistaModel = require('../../models/contratistas');
const obraModel = require('../../models/obra');
const utils = require('../api/utils');



router.get('/listaDeRayas/:idObra/:idContratista', async (req, res) => {

  try {
    const semana = utils.numberWeek(new Date());
    // data lista empleados
    const { idObra, idContratista } = req.params;
    const data = await listaEmpleados
      .findOne({
        obra: mongoose.Types.ObjectId(idObra),
        contratista: mongoose.Types.ObjectId(idContratista),
      })
      .populate({
        path: 'lista',
        populate: {
          path: 'category',
        },
      })
      .exec();
    // Data Contratista
    const dataContratistas = await contratistaModel
      .findOne({ _id: idContratista })
      .exec();
    // data obra
    const dataObra = await obraModel.findOne({ _id: idObra }).exec();
    const employees = data.lista;
    // Create a reusable style
    
const wb = new xl.Workbook();

const headerStyle = wb.createStyle({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: '#ABABA1',
      fgColor: '#ABABA1',
    },
    font: {
      bold: true,
      name: 'Arial',
      size: 10,
    },
  });
  
  const dataStyle = wb.createStyle({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
    font: {
      name: 'Ariel',
      size: 10,
    },
  });
  const totalStyle = wb.createStyle({
    alignment: {
      horizontal: 'left',
    },
    font: {
      name: 'Ariel',
      size: 10,
    },
  });
  const totalBoldStyle = wb.createStyle({
    alignment: {
      horizontal: 'left',
    },
    font: {
      name: 'Ariel',
      size: 10,
      bold: true
    },
  });
  const dataGreenStyle = wb.createStyle({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
    font: {
      name: 'Ariel',
      size: 10,
      color: '#46A921',
    },
  });
  const contratistaStyle = wb.createStyle({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: '#ABABA1',
      fgColor: '#ABABA1',
    },
    font: {
      name: 'Ariel',
      size: 10,
      color: '#808080',
    },
  });
  const datosEmpresaStyle = wb.createStyle({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
    font: {
      name: 'Ariel',
      size: 10,
      bold: true,
    },
  });
  
    // Add Worksheets to the workbook
    const ws = wb.addWorksheet('Lista de raya');
    ws.cell(8, 1)
      .string('Nombre')
      .style(headerStyle);
    ws.cell(8, 2)
      .string('2o Nombre')
      .style(headerStyle);
    ws.cell(8, 3)
      .string('Apellido P')
      .style(headerStyle);
    ws.cell(8, 4)
      .string('Apellido M')
      .style(headerStyle);
    ws.cell(8, 5)
      .string('CURP')
      .style(headerStyle);
    ws.cell(8, 6)
      .string('# IMSS')
      .style(headerStyle);
    ws.cell(8, 7)
      .string('Estatus IMSS')
      .style(headerStyle);
    ws.cell(8, 8)
      .string('Categoría')
      .style(headerStyle);
    ws.cell(8, 9)
      .string('Neto HEG')
      .style(headerStyle);
    ws.cell(8, 10)
      .string('SDI')
      .style(headerStyle);
    ws.cell(8, 11)
      .string('x 1.55')
      .style(headerStyle);
    // Header
    ws.cell(7, 1, 7, 8, true)
      .string(
        `${dataContratistas.nombre.toUpperCase()} ${dataContratistas.apellidoP.toUpperCase()} ${dataContratistas.apellidoM.toUpperCase()}`,
      )
      .style(contratistaStyle);

    ws.cell(7, 9, 7, 11, true)
      .string('ACTIVIDAD')
      .style(contratistaStyle);
    // Datos Generales
    ws.cell(1, 7)
      .string('COMPAÑÍA')
      .style(datosEmpresaStyle);

    ws.cell(2, 7)
      .string('PROYECTO')
      .style(datosEmpresaStyle);
    ws.cell(3, 7)
      .string('CONCEPTO')
      .style(datosEmpresaStyle);
    ws.cell(4, 7)
      .string('FECHA')
      .style(datosEmpresaStyle);
    let size = employees.length;
    ws.cell(size + 11, 7)
      .string('LISTA DE RAYA SEMANAL')
      .style(totalBoldStyle);
    ws.cell(size + 12, 7)
      .string('Esta semana')
      .style(totalStyle);
    ws.cell(size + 13, 7)
      .string('Acumulado Anterior')
      .style(totalStyle);
    ws.cell(size + 14, 7)
      .string('Acumulado Actual')
      .style(totalBoldStyle);
    
      let totalSemana = 0;
      for (const item of employees) {
          totalSemana += item.category.monto;
      }
      console.log(`Comooo? ${totalSemana}`);
    ws.cell(size + 12, 8)
      .string(totalSemana.toString())
      .style(totalStyle);
    ws.cell(size + 13, 8)
      .string(totalSemana.toString())
      .style(totalStyle);
    ws.cell(size + 14, 8)
      .string(totalSemana.toString())
      .style(totalBoldStyle);


    for (let i = 0; i < employees.length; i += 1) {
      ws.cell(i + 9, 1)
        .string(employees[i].name.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 2)
        .string('')
        .style(dataStyle);
      ws.cell(i + 9, 3)
        .string(employees[i].aPaterno.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 4)
        .string(employees[i].aMaterno.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 5)
        .string(employees[i].curp.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 6)
        .string(employees[i].noSocial.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 7)
        .string(employees[i].status ? 'ACTIVO' : 'INACTIVO')
        .style(dataGreenStyle);
      ws.cell(i + 9, 8)
        .string(employees[i].category.name.toUpperCase())
        .style(dataStyle);
      ws.cell(i + 9, 9)
        .number(employees[i].category.monto)
        .style(dataStyle);
      ws.cell(i + 9, 10)
        .number(0)
        .style(dataStyle);
      ws.cell(i + 9, 11)
        .number(0)
        .style(dataStyle);
    }
    const file = `${__dirname}/files/${semana}-${dataContratistas.nombre.toUpperCase()} ${dataContratistas.apellidoP.toUpperCase()}.xlsx`;
    wb.write(file);
    res.download(file);
  } catch (err) {
    res.send(err);
  }
});

// LISTA PERSONAL

router.get('/listaDePersonal/:idObra/', async (req, res) => {
  try {
    // data lista empleados
    const { idObra } = req.params;
    const empleadosObra = await listaEmpleados
      .find({
        obra: mongoose.Types.ObjectId(idObra),
      })
      .populate({
        path: 'lista',
        populate: {
          path: 'category',
        },
      })
      .populate('contratista')
      .exec();
    // Data Contratista
    // data obra
    let listasJuntas = [];
    for (let i = 0; i < empleadosObra.length; i++) {
      listasJuntas = listasJuntas.concat(empleadosObra[i].lista);
    }
    const wb2 = new xl.Workbook();
    const headerStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        fill: {
          type: 'pattern',
          patternType: 'solid',
          bgColor: '#ABABA1',
          fgColor: '#ABABA1',
        },
        font: {
          bold: true,
          name: 'Arial',
          size: 10,
        },
      });
      
      const dataStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        font: {
          name: 'Ariel',
          size: 10,
        },
      });
      const dataGreenStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        font: {
          name: 'Ariel',
          size: 10,
          color: '#46A921',
        },
      });
      const dataRedStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        font: {
          name: 'Ariel',
          size: 10,
          color: '#ff0000',
        },
      });
      const contratistaStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        fill: {
          type: 'pattern',
          patternType: 'solid',
          bgColor: '#ABABA1',
          fgColor: '#ABABA1',
        },
        font: {
          name: 'Ariel',
          size: 10,
          color: '#808080',
        },
      });
      const datosEmpresaStyle = wb2.createStyle({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        font: {
          name: 'Ariel',
          size: 10,
          bold: true,
        },
      });
    const ws2 = wb2.addWorksheet('Lista de empledos');
    ws2.cell(8, 1)
      .string('Nombre')
      .style(headerStyle);
    ws2.cell(8, 2)
      .string('2o Nombre')
      .style(headerStyle);
    ws2.cell(8, 3)
      .string('Apellido P')
      .style(headerStyle);
    ws2.cell(8, 4)
      .string('Apellido M')
      .style(headerStyle);
    ws2.cell(8, 5)
      .string('CURP')
      .style(headerStyle);
    ws2.cell(8, 6)
      .string('# IMSS')
      .style(headerStyle);
    ws2.cell(8, 7)
      .string('Estatus IMSS')
      .style(headerStyle);
    ws2.cell(8, 8)
      .string('Categoría')
      .style(headerStyle);
    ws2.cell(8, 9)
      .string('Neto HEG')
      .style(headerStyle);
    ws2.cell(8, 10)
      .string('ISR')
      .style(headerStyle);
    // Header
    ws2.cell(7, 9, 7, 10, true)
      .string('ACTIVIDAD')
      .style(contratistaStyle);
    // Datos Generales
    ws2.cell(1, 7)
      .string('COMPAÑÍA')
      .style(datosEmpresaStyle);

    ws2.cell(2, 7)
      .string('PROYECTO')
      .style(datosEmpresaStyle);
    ws2.cell(3, 7)
      .string('CONCEPTO')
      .style(datosEmpresaStyle);
    ws2.cell(4, 7)
      .string('FECHA')
      .style(datosEmpresaStyle);
      let employees = listasJuntas;
      for (let i = 0; i < employees.length; i += 1) {
          console.log(employees[i]);
        ws2.cell(i + 9, 1)
          .string(employees[i].name.toUpperCase())
          .style(dataStyle);
        ws2.cell(i + 9, 2)
          .string('')
          .style(dataStyle);
        ws2.cell(i + 9, 3)
          .string(employees[i].aPaterno.toUpperCase())
          .style(dataStyle);
        ws2.cell(i + 9, 4)
          .string(employees[i].aMaterno.toUpperCase())
          .style(dataStyle);
        ws2.cell(i + 9, 5)
          .string(employees[i].curp.toUpperCase())
          .style(dataStyle);
        ws2.cell(i + 9, 6)
          .string(employees[i].noSocial.toUpperCase())
          .style(dataStyle);
        if(employees[i].status){
            ws2.cell(i + 9, 7)
          .string('ACTIVO' )
          .style(dataGreenStyle);
        }else{
            ws2.cell(i + 9, 7)
            .string('BAJA')
            .style(dataRedStyle);
        }
        ws2.cell(i + 9, 8)
          .string(employees[i].category.name.toUpperCase())
          .style(dataStyle);
        ws2.cell(i + 9, 9)
          .number(employees[i].category.monto)
          .style(dataStyle);
        ws2.cell(i + 9, 10)
          .number(0)
          .style(dataStyle);
      }
    const file2 = `${__dirname}/files/listaEmpleados-semana.xlsx`;
    wb2.write(file2);
    res.download(file2);
  } catch (err) {
    res.send(err);
  }
});

module.exports = router;
