const express = require('express');

const router = express.Router();

const EstimacionModel = require('../../models/estimacion');
const utils = require('../api/utils');
const CatalogModel = require('../../models/catalog');

const reducer = (accumulator, currentValue) => accumulator + (currentValue.cantidad * currentValue.cu);

router
  .get('/getByCatalogo/:idCatalogo', async (req, res) => {
    try {
      const estimaciones = await EstimacionModel.find({
        catalogo: req.params.idCatalogo,
        deleted: false,
      })
        .populate({
          path: 'catalogo',
          populate: {
            path: 'conceptos.concepto',
          }
        })
        .exec();
      if (estimaciones) {
        res.status(200).send(estimaciones);
      } else {
        res.status(404).send('No se encontró ningun estimación');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el catálogo ${err}`);
    }
  })
  .get('/:idObra/:idContratista', async (req, res) => {
    try {
      const estimaciones = await EstimacionModel.find({
        obra: req.params.idObra,
        contratista: req.params.idContratista,
        deleted: false,
      })
        .populate({
          path: 'catalogo',
          populate: {
            path: 'conceptos.concepto',
          },
        })
        .exec();
      if (estimaciones) {
        res.status(200).send(estimaciones);
      } else {
        res.status(404).send('No se encontró ningun estimación');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el catálogo ${err}`);
    }
  })
  .get('/:idEstimacion/', (req, res) => {
    Promise.resolve(EstimacionModel.findOne({ deleted: false, _id: req.params.idEstimacion }).populate('catalogo')
      .populate({
        path: 'catalogo',
        populate: {
          path: 'conceptos.concepto',
        }
      }).exec()
      .then((estimaciones) => {
        res.status(200).send(estimaciones);
      }));
  })
  .post('/', async (req, res) => {
    const { catalogo, obra, contratista } = req.body;
    const estimacion = new EstimacionModel(req.body);
    estimacion.semana = utils.numberWeek(new Date());
    estimacion.fecha = new Date();
    let total = 0;
    const catalog = await CatalogModel.findOne({
      _id: catalogo,
      deleted: false,
    }).exec();
    for (const item of catalog.conceptos) {
      total += item.cantidad * item.cu;
    }
    const lastEstimacion = await EstimacionModel.findOne({ deleted: false, obra, contratista }).sort({ createdAt: -1 }).exec();
    estimacion.monto = total;
    if (lastEstimacion == undefined) {
      estimacion.acumulado = total;
      estimacion.total = total;
    } else {
      estimacion.acumulado = lastEstimacion.total;
      console.log(`Total: ${total} Acumulado: ${lastEstimacion.total}`);
      estimacion.total = parseInt(total) + parseInt(lastEstimacion.total);
    }
    catalog.estimacion = true;
    Promise.resolve(estimacion.save())
      .then((estimacion) => {
        // Save estimacion flag in catalog to true
        Promise.resolve(catalog.save()).then(() => {
          res.status(201).send(estimacion);
        });
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  })
  .put('/:idEstimacion/', (req, res) => {
    Promise.resolve(EstimacionModel.findOne({ _id: req.params.idEstimacion })
      .then((estimacion) => {
        const { monto } = req.body;
        console.log(`Data ${req.body}`);
        estimacion.monto = monto || estimacion.monto;
        Promise.resolve(estimacion.save()
          .then((estimacionSaved) => {
            res.status(200).send(estimacionSaved);
          })
          .catch((err) => {
            res.status(500).send(err);
          }));
      }));
  })
  .delete('/:idEstimacion/', (req, res) => {
    Promise.resolve(EstimacionModel.findOne({ deleted: false, _id: req.params.idEstimacion })
      .then((estimacion) => {
        const { data } = req.body;
        estimacion.deleted = !estimacion.deleted;
        Promise.resolve(estimacion.save()
          .then((estimacionSaved) => {
            res.status(200).send(estimacionSaved);
          })
          .catch((err) => {
            res.status(500).send(err);
          }));
      }));
  });


module.exports = router;
