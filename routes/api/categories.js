/* eslint-disable no-param-reassign */
/**
 * routes/api/categories.js
 *
 * @description :: Describes the categories routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
// const debug = require('debug')('backend-constructora:db');
const mongoose = require('mongoose');
const CategoriesModel = require('../../models/categories');

router
  /* GET all the categories */
  .get('/', async (req, res) => {
    const allCategories = await CategoriesModel.find({ deleted: false });
    for(category of allCategories){
      console.log(category._id);
    }
    res.status(200).send(allCategories);
  })

  /* Create a new category */
  .post('/', (req, res) => {
    const category = new CategoriesModel(req.body); 
    Promise.resolve(category.save())
      .then(() => {
        res.status(201).send(category);
      });
  });

router
  /* Get category */
  .get('/:id', (req, res) => {
    Promise.resolve(CategoriesModel.find({ _id: req.params.id, deleted: false }))
      .then((categories) => {
        if (categories.length) {
          res.status(200).send(categories[0]);
        } else {
          res.status(200).send('No se ha encontrado la categoría');
        }
      }).catch((error) => res.status(400).send(`Ha ocurrido un problema al buscar la categoría ${error}`));
  })
  /* Update category */
  .put('/:id', (req, res) => {
    CategoriesModel.findById(mongoose.Types.ObjectId(req.params.id), (error, category) => {
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
      } else if (category) {
        category.monto = req.body.monto || category.monto;
        category.name = req.body.name || category.name;
        category.sdi = req.body.sdi || category.sdi;
        category.average = req.body.average || category.average;
        category.save((err, categorySaved) => {
          res.status(201).send(categorySaved);
        });
      } else {
        res.status(404).send('No se ha encontrado la categoría');
      }
    });
  })
  /* Delete category */
  .delete('/:id', (req, res) => {
    CategoriesModel.findById(mongoose.Types.ObjectId(req.params.id), (error, category) => {  
      if (error) {
        res.status(500).send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
      } else if (category) {
        category.deleted = true;
        category.save((err, caterySaved) =>{
          res.status(201).send(caterySaved);
        });
      } else {
        res.status(200).send('No se ha encontrado la categoría');
      }
    });
  });

module.exports = router;