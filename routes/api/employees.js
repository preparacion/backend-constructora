/**
 * routes/api/categories.js
 *
 * @description :: Describes the categories routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
const debug = require('debug')('backend-constructora:db');
const EmployeesModel = require('../../models/employees');
const listaEmpleados = require('../../models/listaEmpleados');
const ContratistasModel = require('../../models/contratistas');

router
  /* GET all the categories */
  .get('/', (req, res) => {
    Promise.resolve(EmployeesModel.find({ deleted: false })).then(
      (categories) => {
        res.status(200).send(categories);
      },
    );
  })
  /* Create a new employee */
  .post('/', async (req, res) => {
    const employeeExists = await EmployeesModel.findOne({ noSocial: req.body.noSocial, deleted: false });
    if (employeeExists) {
      res.status(403).send('Ya existe dicho empleado');
    } else {
      const employee = new EmployeesModel(req.body);
      if (req.body.categoryName.includes('CONTRATISTA')) {
        const contratista = new ContratistasModel(req.body);
        Promise.resolve(employee.save()).then((employee) => {
          contratista.employee = employee._id;
          Promise.resolve(contratista.save()).then((contratistaSalvado) => {
            Promise.resolve(
              listaEmpleados.findOne({
                contratista: contratistaSalvado._id,
                obra: req.body.obra,
              }),
            ).then((lista) => {
              if (lista) {
                lista.lista.push(employee._id);
                Promise.resolve(lista.save()).then((lista) => {
                  res.status(201).send(lista);
                });
              } else {
                const listaEmployees = new listaEmpleados({
                  contratista: contratista._id,
                  obra: req.body.obra,
                  lista: [employee._id],
                });
                Promise.resolve(listaEmployees.save()).then((lista) => {
                  res.status(201).send(lista);
                });
              }
            });
          });
        });
      } else {
        Promise.resolve(employee.save()).then((employee) => {
          Promise.resolve(
            listaEmpleados.findOne({
              contratista: req.body.contratista,
              obra: req.body.obra,
            }),
          ).then((lista) => {
            if (lista) {
              lista.lista.push(employee._id);
              Promise.resolve(lista.save()).then((lista) => {
                res.status(201).send(lista);
              });
            } else {
              const listaEmployees = new listaEmpleados({
                contratista: req.body.contratista,
                obra: req.body.obra,
                lista: [employee._id],
              });
              Promise.resolve(listaEmployees.save()).then((lista) => {
                res.status(201).send(lista);
              });
            }
          });
        });
      }
    }
  });

router
  /* Get employee */
  .get('/:id', async (req, res) => {
    try {
      const empleado = await EmployeesModel.findOne({
        _id: req.params.id,
        deleted: false,
      })
        .populate('category', 'name monto')
        .populate('obra', 'name')
        .populate('contratista', 'nombre')
        .exec();
      if (empleado) {
        res.status(200).send(empleado);
      } else {
        res.status(404).send('No se encontró el empleado');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el empleado ${err}`);
    }
  })

  /* Update employee */
  .put('/:id', (req, res) => {
    EmployeesModel.findById(req.params.id, async (error, employee) => {
      if (error) {
        res
          .status(500)
          .send(`Ha ocurrido un problema al eliminar el empleado ${error}`);
      } else if (employee) {
        employee.name = req.body.name || employee.name;
        employee.secondName = req.body.secondName || employee.secondName;
        employee.aMaterno = req.body.aMaterno || employee.aMaterno;
        employee.aPaterno = req.body.aPaterno || employee.aPaterno;
        employee.noSocial = req.body.noSocial || employee.noSocial;
        employee.curp = req.body.curp || employee.curp;
        employee.status = req.body.status || employee.status;
        employee.category = req.body.category || employee.category;
        if (employee.obra != req.body.obra || employee.contratista != req.body.contratista) {
          const listaExistente = await listaEmpleados.findOne({
            contratista: employee.contratista,
            obra: employee.obra,
          });
          if (listaExistente) {
            const indexElemento = listaExistente.lista.indexOf(employee._id);
            console.log(listaExistente.lista);
            listaExistente.lista.splice(indexElemento, 1);
            console.log(listaExistente.lista);
            await listaExistente.save();
          }
          // eliminamos el empleado de la lista de empleados
          // buscamos lista para cambiar al empleado
          const listaCambio = await listaEmpleados.findOne({
            contratista: req.body.contratista,
            obra: req.body.obra,
          });
          if (listaCambio) {
            listaCambio.lista.push(employee._id);
            employee.obra = req.body.obra || employee.obra;
            employee.contratista = req.body.contratista || employee.contratista;
            listaCambio.save((lista) => {
              employee.save((err, employeeSaved) => {
                res.status(201).send(employeeSaved);
              });
            });
          } else {
            const listaNueva = new listaEmpleados({
              contratista: req.body.contratista,
              obra: req.body.obra,
              lista: [employee._id],
            });
            employee.obra = req.body.obra || employee.obra;
            employee.contratista = req.body.contratista || employee.contratista;
            listaNueva.save((lista) => {
              employee.save((err, employeeSaved) => {
                res.status(201).send(employeeSaved);
              });
            });
          }
        } else {
          employee.obra = req.body.obra || employee.obra;
          employee.contratista = req.body.contratista || employee.contratista;
          employee.save((err, employeeSaved) => {
            res.status(201).send(employeeSaved);
          });
        }
      } else {
        res.status(404).send('No se ha encontrado la empleado');
      }
    });
  })
  /* Update employees status */
  .put('/status/:id', (req, res) => {
    EmployeesModel.findById(req.params.id, (error, employee) => {
      if (error) {
        res
          .status(500)
          .send(`Ha ocurrido un problema al editar el empleado ${error}`);
      } else if (employee) {
        console.log(`Status: ${employee.status}`);
        employee.status = !employee.status;
        employee.save((err, employeeSaved) => {
          res.status(201).send(employeeSaved);
        });
      } else {
        res.status(404).send('No se ha encontrado la empleado');
      }
    });
  })
  /* Add comment to employee */
  .put('/addComment/:id', (req, res) => {
    EmployeesModel.findById(req.params.id, (error, employee) => {
      if (error) {
        res
          .status(500)
          .send(`Ha ocurrido un problema al eliminar el empleado ${error}`);
      } else if (employee) {
        employee.comments.push({ comment: req.body.comment });
        employee.save((err, employeeSaved) => {
          res.status(201).send(employeeSaved);
        });
      } else {
        res.status(404).send('No se ha encontrado la empleado');
      }
    });
  })
  /* Delete category */
  .delete('/:id', (req, res) => {
    EmployeesModel.findById(req.params.id, (error, employee) => {
      if (error) {
        res
          .status(500)
          .send(`Ha ocurrido un problema al eliminar la categoría ${error}`);
      } else if (employee) {
        employee.deleted = true;
        employee.save((err, employeeSaved) => {
          res.status(201).send(employeeSaved);
        });
      } else {
        res.status(200).send('No se ha encontrado el empleado');
      }
    });
  });

module.exports = router;
