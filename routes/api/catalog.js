/**
 * routes/api/categories.js
 *
 * @description :: Describes the catalog routes
 * @docs        :: TODO
 */
const express = require('express');

const router = express.Router();
const debug = require('debug')('backend-constructora:db');
const CatalogModel = require('../../models/catalog');
const EstimacionModel = require('../../models/estimacion');
const conceptModel = require('../../models/concepts');
const parseDecimalNumber = require('parse-decimal-number');

const fonts = {
  Roboto: {
    normal: 'fonts/Roboto-Regular.ttf',
    bold: 'fonts/Roboto-Medium.ttf',
    italics: 'fonts/Roboto-Italic.ttf',
    bolditalics: 'fonts/Roboto-MediumItalic.ttf',
  },
};

const PdfPrinter = require('pdfmake');

const printer = new PdfPrinter(fonts);
const fs = require('fs');
// const listaEmpleados = require("../../models/listaEmpleados");

router
/* GET all the concepts */
  .get('/', (req, res) => {
    Promise.resolve(CatalogModel.find({ deleted: false }))
      .then((catalog) => {
        res.send(catalog);
      });
  })
  /* Create a new catalog */
  .post('/', (req, res) => {
    const catalog = new CatalogModel(req.body);
    Promise.resolve(catalog.save()).then(() => {
      res.status(201).send(catalog);
    });
  });

router
  /* Get catalog */
  .get('/:id', async (req, res) => {
    try {
      const catalog = await CatalogModel.findOne({
        _id: req.params.id,
        deleted: false,
      })
        .populate('conceptos.concepto')
        // .populate('obra')
        // .populate('contratista')
        .exec();
      if (catalog) {
        res.status(200).send(catalog);
      } else {
        res.status(404).send('No se encontró el empleado');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el catálogo ${err}`);
    }
  })
  // GET CATALOG BY OBRA
  /* Get catalog */
  .get('/obra/:idObra/', async (req, res) => {
    try {
      const catalog = await CatalogModel.find({
        obra: req.params.idObra,
        deleted: false,
      })
        .populate('conceptos.concepto')
        // .populate('obra')
        // .populate('contratista')
        .exec();
      if (catalog) {
        const catalogList = catalog;
        const newCatalog = catalog[catalog.length - 1];
        newCatalog.conceptos.map((item) => {
          item.cantidadEstimacionActual = 0;
          item.importeEstimacionActual = 0;
          item.cantidadAcumuladoAnterior = item.cantidadAcumulado || 0;
          item.importeAcumuladoAnterior = item.importeAcumulado || 0;
        });
        catalogList.push(newCatalog);
        res.status(200).send(catalog);
      } else {
        res.status(404).send('No se encontró el empleado');
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el catálogo ${err}`);
    }
  })
  /* Get catalog */
  .get('/:idObra/:idContratista', async (req, res) => {
    try {
      const catalog = await CatalogModel.find({
        obra: req.params.idObra,
        contratista: req.params.idContratista,
        deleted: false,
      })
        .populate('conceptos.concepto')
        // .populate('obra')
        // .populate('contratista')
        .exec();
      if (catalog.length > 0) {
        const catalogList = catalog;
        const newCatalog = catalog[catalog.length - 1];
        newCatalog.conceptos.map((item) => {
          item.cantidadEstimacionActual = 0;
          item.importeEstimacionActual = 0;
          item.cantidadAcumuladoAnterior = item.cantidadAcumulado || 0;
          item.importeAcumuladoAnterior = item.importeAcumulado || 0;
        });
        catalogList.push(newCatalog);
        res.status(200).send(catalog);
      } else {
        res.status(200).send([]);
      }
    } catch (err) {
      res.status(500).send(`Ocurrió un error al buscar el catálogo ${err}`);
    }
  })

  /* Add concept to catalog */
  .put('/addConcept/:id', async (req, res) => {
    const estimacion = await EstimacionModel.findOne({ deleted: false, catalogo: req.params.id });
    if (estimacion != undefined) {
      res
        .status(500)
        .send('Ya se ha generado una estimación con este catálogo ya no se pueden realizar cambios');
    } else {
      CatalogModel.findById(req.params.id, async (error, catalog) => {
        if (error) {
          res
            .status(500)
            .send(`Ha ocurrido un problema al agregar el concepto ${error}`);
        } else if (catalog) {
          const { concept } = req.body;
          const conceptData = await conceptModel.findById(concept.concepto);
          concept.importe = concept.cantidad * conceptData.pu;
          catalog.conceptos.push(concept);
          catalog.save((err, catalogSaved) => {
            res.status(201).send(catalogSaved);
          });
        } else {
          res.status(404).send('No se ha encontrado el catálogo');
        }
      });
    }
  })
  /* Add concept to catalog */
  
  /* Delete category */
  .delete('/deleteConcept/:idCatalog/:idConcept', async (req, res) => {
    const estimacion = await EstimacionModel.findOne({ deleted: false, catalogo: req.params.idCatalog });
    if (estimacion != undefined) {
      res
        .status(500)
        .send('Ya se ha generado una estimación con este catálogo ya no se pueden realizar cambios');
    } else {
      CatalogModel.findById(req.params.idCatalog).populate('conceptos.concepto').exec((error, catalog) => {
        if (error) {
          res
            .status(500)
            .send(`Ha ocurrido un problema al eliminar el concepto ${error}`);
        } else if (catalog) {
          const conceptToEdit = catalog.conceptos;
          const position = conceptToEdit.findIndex((item) => item._id == req.params.idConcept);
          conceptToEdit.splice(position, 1);
          catalog.save((err, catalogSaved) => {
            res.status(201).send(catalogSaved);
          });
        } else {
          res.status(404).send('No se ha encontrado el catálogo');
        }
      });
    }
  })
  /* Delete category */
  .delete('/deleteConcept/:idCatalog/:idConcept', async (req, res) => {
    CatalogModel.findById(req.params.idCatalog).populate('conceptos.concepto').exec((error, catalog) => {
      if (error) {
        res
          .status(500)
          .send(`Ha ocurrido un problema al eliminar el concepto ${error}`);
      } else if (catalog) {
        const conceptToEdit = catalog.conceptos;
        const position = conceptToEdit.findIndex((item) => item._id == req.params.idConcept);
        conceptToEdit.splice(position, 1);
        catalog.save((err, catalogSaved) => {
          res.status(201).send(catalogSaved);
        });
      } else {
        res.status(404).send('No se ha encontrado el catálogo');
      }
    });
  });

  router
    .put('/editConcept/:idCatalog/:idConcept', async (req, res) => {
    res.status(200).send('ah va');

    const estimacion = await EstimacionModel.findOne({ deleted: false, catalogo: req.params.idCatalog });
    if (estimacion != undefined) {
      res
        .status(500)
        .send('Ya se ha generado una estimación con este catálogo ya no se pueden realizar cambios');
    } else {
      CatalogModel.findById(req.params.idCatalog).populate('conceptos.concepto').exec( async (error, catalog) => {
        if (error) {
          res
            .status(500)
            .send(`Ha ocurrido un problema al agregar el concepto ${error}`);
        } else if (catalog) {
          const conceptToEdit = catalog.conceptos.find((item) => item._id == req.params.idConcept);
          conceptToEdit.cantidad = req.body.concept.cantidad || conceptToEdit.cantidad;
          conceptToEdit.importe = req.body.concept.importe;
          conceptToEdit.cantidadAcumulado = parseDecimalNumber(req.body.concept.cantidadAcumulado) || conceptToEdit.cantidadAcumulado;
          conceptToEdit.importeAcumulado = parseDecimalNumber(req.body.concept.importeAcumulado) || conceptToEdit.importeAcumulado;
          conceptToEdit.cantidadAcumuladoAnterior = parseDecimalNumber(req.body.concept.cantidadAcumuladoAnterior) || conceptToEdit.cantidadAcumuladoAnterior;
          conceptToEdit.importeAcumuladoAnterior = parseDecimalNumber(req.body.concept.importeAcumuladoAnterior) || conceptToEdit.importeAcumuladoAnterior;
          conceptToEdit.porcentaje = parseDecimalNumber(req.body.concept.porcentaje) || conceptToEdit.porcentaje;
          conceptToEdit.cantidadEstimacionActual = parseDecimalNumber(req.body.concept.cantidadEstimacionActual) || conceptToEdit.cantidadEstimacionActual;
          conceptToEdit.importeEstimacionActual = parseDecimalNumber(req.body.concept.importeEstimacionActual) || conceptToEdit.importeEstimacionActual;
          catalog.save((err, catalogSaved) => {
            console.log('Si entra');
              res
                .send({"message":"post done"});
          
          });
        } else {
          res.status(404).send('No se ha encontrado el catálogo');
        }
      });
    }
  })
module.exports = router;
