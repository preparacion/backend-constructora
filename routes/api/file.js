const express = require('express');

const router = express.Router();
const uploadImage = require('../../helpers/uploadImage');
const EmployeesModel = require('../../models/employees');
const ContratistasModel = require('../../models/contratistas');

router
  .post('/', async (req, res, next) => {
    const { employee, contratista, name } = req.body;
    try {
      const myFile = req.file;
      const imageUrl = await uploadImage(myFile, name);
      if (employee !== undefined) {
        EmployeesModel.findById(employee, (error, employee) => {
          if (error) {
            res
              .status(500)
              .send(`Ha ocurrido un problema al eliminar el empleado ${error}`);
          } else if (employee) {
            if (employee.files) {
              employee.files.push({ url: imageUrl, name });
            } else {
              employee.files = [{ url: imageUrl, name }];
            }
            employee.save((err, employeeSaved) => {
              res
                .status(200)
                .json({
                  message: 'Upload was successful',
                  data: employeeSaved,
                });
            });
          }
        });
      } else {
        ContratistasModel.findById(contratista, (error, contratista) => {
          if (error) {
            res
              .status(500)
              .send(`Ha ocurrido un problema al eliminar el empleado ${error}`);
          } else if (contratista) {
            console.log(contratista);
            if (contratista.files) {
              contratista.files.push({ url: imageUrl, name });
            } else {
              contratista.files = [{ url: imageUrl, name }];
            }
            EmployeesModel.findById(contratista.employee, (error, employeeData) => {
              console.log(contratista);
              employeeData.files = contratista.files;
              if (error) {
                res
                  .status(500)
                  .send(`Ha ocurrido un problema al agregar el archivo ${error}`);
              } else if (employeeData) {
                employeeData.save((err, employeeSaved) => {
                    contratista.save((err, contratistaSaved) => {
                    res
                      .status(200)
                      .json({
                        message: 'Upload was successful',
                        data: contratistaSaved,
                      });
                  });
                });
              }
            });
          }
        });
      }
    } catch (error) {
      res.send(`Error ${error}`);
      console.log(`Error ${error}`);
    }
  });
module.exports = router;
