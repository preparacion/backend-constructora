# backend-constructora

## Deployment

### 1. Install packages
```
$ sudo apt install linux-headers-$(uname -r) build-essential curl wget git
```

### 2. Install NVM
```
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

$ nvm install 10.16.3
```

### 3. Install Nginx
```
$ sudo apt install nginx ufw

$ sudo ufw allow 'Nginx HTTP' 

$ cd /etc/nginx/sites-enabled

$ sudo nano default
```

Copy and paste the next lines in the "location" sections

```

sudo rm /etc/nginx/sites-enabled/default

sudo nano /etc/nginx/sites-available/node

copy and paste next :

server {
    listen 80;
    server_name servername.com;

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         "http://127.0.0.1:3000";
    }
}


sudo ln -s /etc/nginx/sites-available/node /etc/nginx/sites-enabled/node
sudo service nginx restart
```

### 4. Clone this repo

```
$ npm i pm2 -g

$ cd

$ git clone https://bluedotsoft@bitbucket.org/preparacion/backend-constructora.git

$ backend-constructora

$ pm2 install pm2-logrotate

$ npm i

$ npm i socket.io --save

mkdir .envs && cd .envs

touch production

(copiar y pegar las variables de entorno)


```

### 5 Install https Certs

```
$ sudo apt-get update

$ sudo apt-get install software-properties-common

$ sudo add-apt-repository ppa:certbot/certbot

$ sudo apt-get update

$ sudo apt-get install certbot python-certbot-nginx

$ sudo certbot --nginx

Yes Terms and conditions 

enter the next email when ask moisescorreo@outlook.com

then enter the domain names (Check if the dns redirects are already pointing to the current ip address)

heg.bluedotmx.com

chose option 2 for redirecta all request to https (make sure you already accept https income request in server / 443 port )

then enable autorenew if success last steps:

$ set autorenew cert 

$ sudo crontab -e

choose nano (option 1)

copy and paste next in end of file 

15 3 * * * /usr/bin/certbot renew --quiet
        


```


## 1. Create .envs directory and add the configuration files

For example: **development**

```
NODE_ENV=development
APP_PORT=3000
IP = localhost

```


