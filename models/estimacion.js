/**
 * models/estimacion.js
 *
 * @description :: Describe estimación & functions
 * @docs        :: None
 */
const db = require("./db");
const Schema = db.Schema;
const mongoose = require("mongoose");

const EstimacionSchema = new Schema(
  {
    fecha: { type: Date, required: true },
    semana: { type: Number },
    monto: { type: Number },
    total: { type: Number },
    acumulado: { type: Number },
    catalogo: { type: mongoose.Schema.Types.ObjectID, ref: 'Catalog' },
    deleted: { type: Boolean, default: false },
    contratista: { type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas', required: true },
    obra: { type: mongoose.Schema.Types.ObjectId, ref: 'Obra', required: true },
  },
  {
    versionKey: false,
    timestamps: true
  }
);

const Estimacion = db.model('Estimacion', EstimacionSchema, 'Estimacion');

module.exports = Estimacion;
