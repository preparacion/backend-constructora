/**
 * models/categories.js
 *
 * @description :: Describe categories schema & functions
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CategoriesSchema = new Schema({
  name: { type: String, required: true },
  monto: { type: Number },
  deleted: { type: Boolean, default: false },
  sdi: { type: Number },
  average: { type: Number },
},
{
  versionKey: false,
  timestamps: true
})

const Categories = db.model('Categories', CategoriesSchema, 'Categories')

module.exports = Categories
