/**
 * models/conecepts.js
 *
 * @description :: Describe estimación & functions
 * @docs        :: None
 */
const db = require("./db");
const Schema = db.Schema;
const mongoose = require("mongoose");

const PagoSchema = new Schema(
  {
    obra: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Obra' }],
    monto: { type: Number, required: true  },
    estimacion:{type: mongoose.Schema.Types.ObjectId, ref:'Estimacion'},
    concepto:{type: String},
    contratista: { type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas'  },
  },
  {
    versionKey: false,
    timestamps: true
  }
);

const Pago = db.model('Pagos', PagoSchema, 'Pagos');

module.exports = Pago;

