/**
 * models/conecepts.js
 *
 * @description :: Describe employees & functions
 * @docs        :: None
 */
const db = require("./db");
const Schema = db.Schema;
const mongoose = require("mongoose");

const ObraSchema = new Schema(
  {
    name: { type: String, required: true },
    address: { type: String, required: false },
    deleted: { type: Boolean, default: false },
    contratistas: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas' }]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

const Obra = db.model('Obra', ObraSchema, 'Obra');

module.exports = Obra;
