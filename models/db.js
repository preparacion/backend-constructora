/**
 * /models/db
 *
 * @description :: Setup the db connection
 * @docs        :: TODO
 */
const mongoose = require('mongoose')
const debug = require('debug')('backend-constructora:db')
const dotenv = require('dotenv');
const env = process.env.NODE_ENV || 'development';
const path = require('path');
let ip = process.env.IP;

const envPath = path.resolve(__dirname, `../.envs/${env}`)
dotenv.config({ path: envPath });

// TODO: import env variables
if (ip !== 'localhost') {
  ip = `${process.env.USR}:${process.env.PASS}@${ip}`;
}
const mongoURI = `mongodb://${ip}:27017/constructora`;


mongoose.connect(mongoURI, { useNewUrlParser: true })

const connection = mongoose.connection
connection.on('error', console.error.bind(console, 'connection error: '))
connection.once('open', function () {
  console.log('db connected!!')
  debug(`DB URL: ${mongoURI}`)
})

module.exports = mongoose
