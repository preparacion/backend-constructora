/**
 * models/conecepts.js
 *
 * @description :: Describe employees & functions
 * @docs        :: None
 */
const db = require("./db");
const Schema = db.Schema;
const mongoose = require("mongoose");

const ContratistasSchema = new Schema(
  {
    name: { type: String, required: true  },
    secondName: { type: String },
    aPaterno: { type: String, required: true },
    aMaterno: { type: String, required: true },
    noSocial: { type: String, required: true },
    rfc: { type: String },
    type: { type: String },
    deleted: { type: Boolean, default: false },
    obras: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Obra' }],
    files:[{
      date: { type: Date, default: Date.now },
      name: { type: String, required: true },
      url: { type: String, required: true },
    }],
    employee: { type: mongoose.Schema.Types.ObjectId, ref: 'Obra' }
  },
  {
    versionKey: false,
    timestamps: true
  }
);
const Contratistas = db.model('Contratistas', ContratistasSchema, 'Contratistas');

module.exports = Contratistas;
