/**
 * models/conecepts.js
 *
 * @description :: Describe employees & functions
 * @docs        :: None
 */
const db = require("./db");
const Schema = db.Schema;
const mongoose = require("mongoose");

const listaEmpleadosSchema = new Schema(
  {
    lista:[{ type: mongoose.Schema.Types.ObjectId, ref: 'Employee' }],
    contratista: { type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas' },
    obra: { type: mongoose.Schema.Types.ObjectId, ref: 'Obra' }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

const listaEmpleados = db.model('listaEmpleados', listaEmpleadosSchema, 'listaEmpleados');

module.exports = listaEmpleados;
