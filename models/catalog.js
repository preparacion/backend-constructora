/**
 * models/catalogo.js
 *
 * @description :: Describe catalogo schema & functions
 * @docs        :: None
 */
const db = require('./db');
const Schema = db.Schema;
const mongoose = require('mongoose');


const CatalogSchema = new Schema({
  contratista: { type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas' },
  obra: { type: mongoose.Schema.Types.ObjectId, ref: 'Obra' },
  conceptos: [{ 
    concepto: { type: mongoose.Schema.Types.ObjectId, ref: 'Concepts' },
    cantidad: { type: Number, default: 0 },
    cantidadAcumulado: { type: Number, default: 0 },
    importeAcumulado: { type: Number, default: 0 },
    cantidadAcumuladoAnterior: { type: Number, default: 0 },
    importeAcumuladoAnterior: { type: Number, default: 0 },
    porcentaje: { type: Number, default: 0 },
    importe: { type: Number, default: 0 },
    cantidadEstimacionActual: { type: Number, default: 0 },
    importeEstimacionActual: { type: Number, default: 0 }, 
  }],
  numeroEstimacion: { type: Number, default: 0 },
  extras: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Catalog' }],
  isExtra: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
},
{
  versionKey: false,
  timestamps: true,
});

const Catalog = db.model('Catalog', CatalogSchema, 'Catalog');

module.exports = Catalog;
