/**
 * models/conecepts.js
 *
 * @description :: Describe employees & functions
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema;
const mongoose = require('mongoose')


const EmployeesSchema = new Schema({
  name: { type: String, required: true },
  secondName: { type: String },
  aPaterno: { type: String, required: true },
  aMaterno: { type: String, required: true },
  noSocial: { type: String, required: true },
  curp: { type: String },
  status: { type: Boolean, required: true },
  deleted: { type: Boolean, default: false },
  category: { type: mongoose.Schema.Types.ObjectId, ref: 'Categories' },
  comments: [{ 
    comment: String,
    date: { type: Date, default: Date.now },
  }],
  files:[{
      date: { type: Date, default: Date.now },
      name: { type: String, required: true },
      url: { type: String, required: true },
  }],
  contratista: { type: mongoose.Schema.Types.ObjectId, ref: 'Contratistas' },
  obra: { type: mongoose.Schema.Types.ObjectId, ref: 'Obra' }
},
{
  versionKey: false,
  timestamps: true
})

const Employee = db.model('Employee', EmployeesSchema, 'Employee')

module.exports = Employee;
