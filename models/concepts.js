/**
 * models/concepts.js
 *
 * @description :: Describe concepts schema & functions
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ConceptsSchema = new Schema({
  familia: { type: String, required: true },
  clave: { type: String, required: true },
  pu: { type: Number },
  concepto: { type: String },
  unidad: { type: String },
  deleted: { type: Boolean, default: false },
},
{
  versionKey: false,
  timestamps: true,
});

const Concepts = db.model('Concepts', ConceptsSchema, 'Concepts');

module.exports = Concepts;
