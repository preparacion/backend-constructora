const CatalogoModel = require('../models/catalog');
const express = require('express');
const router = express.Router();

router.get('/:obra/:contratista', async (req, res) =>{
    const { obra, contratista } = req.param;
    const catalogo = await CatalogoModel.find({obra, contratista }).sort({_id:1}).exec();
    res.send(catalogo.numeroEstimacion++);
});



module.exports = router;